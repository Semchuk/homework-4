#include "cinfo.h"
#include <windows.h>

int getWidthConsole()
{
    HANDLE hMyConsole;
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    hMyConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleScreenBufferInfo(hMyConsole, &consoleInfo);
    return (consoleInfo.srWindow.Right - consoleInfo.srWindow.Left + 1);
}

int getHeightConsole()
{
    HANDLE hMyConsole;
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    hMyConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleScreenBufferInfo(hMyConsole, &consoleInfo);
    return (consoleInfo.srWindow.Bottom - consoleInfo.srWindow.Top + 1);
}

void setCursorPosConsole(int _x, int _y)
{
    COORD posCursor;
    HANDLE hMyConsole;
    hMyConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    posCursor.X = _x;
    posCursor.Y = _y;
    SetConsoleCursorPosition(hMyConsole, posCursor);
}

